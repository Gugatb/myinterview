package com.example;

/**
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class Task1 {
	public static void main(String[] args) {
		System.out.println("'madam' is palindrome? " + isPalindrome("madam"));
		System.out.println("'racecar' is palindrome? " + isPalindrome("racecar"));
		System.out.println("'interview' is palindrome? " + isPalindrome("interview"));
	}
	
	/**
	 * Is palindrome?
	 * @author Gugatb
	 * @param pText the text
	 * @return true if palindrome, otherwise false
	 */
	public static boolean isPalindrome(String pText) {
		boolean isPalindrome = false;
		String text = pText;
		
		try {
			text = new StringBuilder(text).reverse().toString();
			isPalindrome = pText.equals(text);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		return isPalindrome;
	}
}
