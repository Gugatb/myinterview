package com.example;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.Set;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 */
public class Task2 {
	// Will iterate in the order in which the entries were put into the map.
	private LinkedHashMap<Long, TaskItem> map;
	
	private TaskItem first;
	private TaskItem last;
	
	public static void main(String[] args) {
		Task2 list = new Task2();
		list.add(1L, "Item A");
		list.add(2L, "Item B");
		list.add(3L, "Item C");
		list.add(4L, "Item D");
		list.add(5L, "Item A");
		list.add(6L, "Item E");
		list.add(7L, "Item A");
		list.add(8L, "Item F");
		
		System.out.println("List: " + list.toString());
		
		list.remove(4L);
		
		System.out.println("List: " + list.toString());
	}
	
	/**
	 * Class constructor.
	 * @author Gugatb
	 */
	public Task2() {
		this.map = new LinkedHashMap<Long, TaskItem>();
	}
	
	/**
	 * Add the item.
	 * @author gugatb
	 * @param pId the id
	 * @param pContent the content
	 * @return added the number of items added
	 */
	public int add(long pId, String pContent) {
		TaskItem item = new TaskItem();
		item.setId(pId);
		item.setContent(pContent);
		return add(item);
	}
	
	/**
	 * Add the item.
	 * @author Gugatb
	 * @param pItem the item
	 * @return added the number of items added
	 */
	public int add(TaskItem pItem) {
		int added = 0;
		
		if (pItem.getId() > 0L && !map.containsKey(pItem.getId())) {
			map.put(pItem.getId(), pItem);
			added++;
			
			if (first == null) {
				first = pItem;
				last = pItem;
			}
			else {
				pItem.setPrevious(last);
				last.setNext(pItem);
				last = pItem;
			}
		}
		return added;
	}
	
	/**
	 * Create the list.
	 * @author Gugatb
	 * @param pLow the low number
	 * @param pHigh the high number
	 * @param pNumber the number
	 */
	public void createList(int pLow, int pHigh, int pNumber) {
		Random random = new Random();
		
		for (int index = 1; index <= pNumber; index++) {
			add(index, "Item " + random.nextInt(pHigh - pLow) + pLow);
		}
	}
	
	/**
	 * Get the first item.
	 * @author Gugatb
	 * @return first the first item
	 */
	public TaskItem getFirst() {
		return first;
	}
	
	/**
	 * Get the item.
	 * @author Gugatb
	 * @param pId the id
	 * @return item the item
	 */
	public TaskItem getItem(long pId) {
		return map.get(pId);
	}
	
	/**
	 * Get the last item.
	 * @author Gugatb
	 * @return last the last item
	 */
	public TaskItem getLast() {
		return last;
	}
	
	/**
	 * Print the items.
	 * @author Gugatb
	 */
	public void print() {
		TaskItem item = first;
		
		while (item != null) {
			item.print();
			item = item.getNext();
			System.out.print(" ");
		}
	}
	
	/**
	 * Remove the item.
	 * @author Gugatb
	 * @param pId the id
	 * @return added the number of items added
	 */
	public int remove(String pContent) {
		Set<Long> ids = new HashSet<Long>();
		int removed = 0;
		
		// Get ids.
		for (TaskItem item : map.values()) {
			if (item.getContent().equalsIgnoreCase(pContent)) {
				ids.add(item.getId());
			}
		}
		
		// Remove items.
		for (Long id : ids) {
			removed += remove(id);
		}
		return removed;
	}
	
	/**
	 * Remove the item.
	 * @author Gugatb
	 * @param pId the id
	 * @return added the number of items added
	 */
	public int remove(long pId) {
		int removed = 0;
		
		if (map.containsKey(pId)) {
			if (first.getId() == pId && first.getNext() != null) {
				first = first.getNext();
				first.setPrevious(null);
			}
			else if (last.getId() == pId && last.getPrevious() != null) {
				last = last.getPrevious();
				last.setNext(null);
			}
			else if (first.getId() != pId && last.getId() != pId) {
				TaskItem item = map.get(pId);
				item.getPrevious().setNext(item.getNext());
			}
			map.remove(pId);
			removed++;
		}
		return removed;
	}
	
	/**
	 * Remove the item.
	 * @author Gugatb
	 * @param pItem the item
	 * @return added the number of items added
	 */
	public int remove(TaskItem pItem) {
		return remove(pItem.getId());
	}
	
	/**
	 * List size.
	 * @author Gugatb
	 * @return size the list size
	 */
	public int size() {
		return map.size();
	}
	
	@Override
	public String toString() {
		TaskItem item = first;
		String string = "";
		
		while (item != null) {
			string += item.toString();
			item = item.getNext();
			string += " ";
		}
		return string;
	}
	
	public class TaskItem {
		private String content;
		private long id;
		private TaskItem next;
		private TaskItem previous;
		
		/**
		 * Class constructor.
		 * @author Gugatb
		 */
		public TaskItem() {
			this.content = "";
			this.id = 0L;
		}
		
		/**
		 * Class constructor.
		 * @author Gugatb
		 * @param pId the id
		 */
		public TaskItem(long pId) {
			this.content = "";
			this.id = pId;
		}
		
		/**
		 * Get the content.
		 * @author gugatb
		 * @return content the content
		 */
		public String getContent() {
			return content;
		}
		
		/**
		 * Get the id.
		 * @author Gugatb
		 * @return id the id
		 */
		public long getId() {
			return id;
		}
		
		/**
		 * Get the next item.
		 * @author Gugatb
		 * @return next the next item
		 */
		public TaskItem getNext() {
			return next;
		}
		
		/**
		 * Get the previous item.
		 * @author Gugatb
		 * @return previous the previous item
		 */
		public TaskItem getPrevious() {
			return previous;
		}
		
		/**
		 * Print the item.
		 * @author Gugatb
		 */
		public void print() {
			System.out.print("[" + id + ": " + content + "]");
		}
		
		/**
		 * Set the content.
		 * @author Gugatb
		 * @param pContent the content
		 */
		public void setContent(String pContent) {
			this.content = pContent;
		}
		
		/**
		 * Set the id.
		 * @author Gugatb
		 * @param pId the id
		 */
		public void setId(long pId) {
			this.id = pId;
		}
		
		/**
		 * Set the next item.
		 * @author Gugatb
		 * @param pNext the next item
		 */
		public void setNext(TaskItem pNext) {
			this.next = pNext;
		}
		
		/**
		 * Set the previous item.
		 * @author Gugatb
		 * @param pPrevious the previous item
		 */
		public void setPrevious(TaskItem pPrevious) {
			this.previous = pPrevious;
		}
		
		@Override
		public String toString() {
			return "[" + id + ": " + content + "]";
		}
	}
}
