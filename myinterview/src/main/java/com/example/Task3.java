package com.example;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 */
public class Task3 extends Task2 {
	public static void main(String[] args) {
		Task3 list = new Task3();
		list.add(1L, "Item A");
		list.add(2L, "Item B");
		list.add(3L, "Item C");
		list.add(4L, "Item D");
		list.add(5L, "Item A");
		list.add(6L, "Item E");
		list.add(7L, "Item A");
		list.add(8L, "Item F");
		
		System.out.println("How many distinct (1): " + list.getHowManyDistinct1());
		System.out.println("How many distinct (2): " + list.getHowManyDistinct2());
	}
	
	/**
	 * Get how many distinct.
	 * @author Gugatb
	 * @return number how many distinct
	 */
	public int getHowManyDistinct1() {
		Set<String> contents = new HashSet<String>();
		TaskItem item = this.getFirst();
		
		while (item != null) {
			contents.add(item.getContent());
			item = item.getNext();
		}
		return contents.size();
	}
	
	/**
	 * Get how many distinct.
	 * @author Gugatb
	 * @return number how many distinct
	 */
	public int getHowManyDistinct2() {
		Map<String, Integer> contents = new HashMap<String, Integer>();
		TaskItem item = this.getFirst();
		int notRepeated = 0;
		
		while (item != null) {
			String content = item.getContent();
			
			if (contents.containsKey(content)) {
				int counter = contents.get(content);
				contents.put(content, counter + 1);
			}
			else {
				contents.put(content, 1);
			}
			item = item.getNext();
		}
		
		// Count the items.
		for (Entry<String, Integer> entry : contents.entrySet()) {
			if (entry.getValue() == 1) {
				notRepeated++;
			}
		}
		return notRepeated;
	}
}
