package com.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Create an implementation of a Rest API client.
 * Prints out how many records exists for each gender and save this file to s3 bucket
 * API endpoint => https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda
 * AWS s3 bucket => interview-digiage
 */
public class Task4 {
	public static void main(String[] args) throws ClientProtocolException, IOException {
		Map<String, Integer> counters = getCounters(
			"https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda"
		);
		
		for (Entry<String, Integer> entry : counters.entrySet()) {
			System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}
	
	/**
	 * Create the file.
	 * @author Gugatb
	 * @param pCounters the counters
	 * @return file the file
	 * @throws IOException
	 */
	private static File createFile(Map<String, Integer> pCounters) throws IOException {
		File file = File.createTempFile("aws", ".txt");
		file.deleteOnExit();
		
		// Create the file.
		Writer writer = new OutputStreamWriter(new FileOutputStream(file));
		writer.write("Female = " + pCounters.get("Female") + "\n");
		writer.write("Male = " + pCounters.get("Male") + "\n");
		writer.close();
		return file;
	}
	
	/**
	 * Get the counters.
	 * @author Gugatb
	 * @return counters the counters
	 */
	@SuppressWarnings("rawtypes")
	public static Map<String, Integer> getCounters(String pEndpoint) {
		Map<String, Integer> counters = new HashMap<String, Integer>();
		Set<String> numbers = new HashSet<String>();
		int female = 0, male = 0;
		
		// Initialize the map.
		counters.put("Female", 0);
		counters.put("Male", 0);
		
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			HttpGet request = new HttpGet(pEndpoint);
			HttpResponse response = httpClient.execute(request);
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			
			// Read the json.
			String json = "", line = "";
			while ((line = reader.readLine()) != null) {
				json += line;
			}
			
			// Parser the json.
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(json);
			JsonArray array = element.getAsJsonArray();
			Iterator iterator = array.iterator();
			
			// Count the elements.
			while (iterator.hasNext()) {
				element = (JsonElement)iterator.next();
				JsonObject object = element.getAsJsonObject();
				String number = object.get("emp_no").getAsString();
				
				if (!numbers.contains(number)) {
					numbers.add(number);
					
					if (object.get("gender") != null && object.get("gender").getAsString().equals("F")) {
						female++;
					}
					else if (object.get("gender") != null && object.get("gender").getAsString().equals("M")) {
						male++;
					}
				}
			}
			
			// Set the counters.
			counters.put("Female", female);
			counters.put("Male", male);
		}
		catch (IOException exception) {
			exception.printStackTrace();
		}
		return counters;
	}
	
	/**
	 * Send to bucket.
	 * @author Gugatb
	 * @param pCounters the counters
	 */
	public void sendToBucket(Map<String, Integer> pCounters) {
		String accessKeyId = "AKIAIN2XDWXSTH7VZN4Q";
		String bucketName = "interview-digiage";
		String objectKey = "AWS";
		String secretAccessKey = "/kMdZX1SC8wBNYmsyecoAzHl16QF5toGHHvyU4Sx";
		
		try {
			AWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
			AmazonS3 client = AmazonS3ClientBuilder
			.standard()
			.withCredentials(new AWSStaticCredentialsProvider(credentials))
			.withRegion(Regions.SA_EAST_1)
			.build();
			
			client.putObject(new PutObjectRequest(bucketName, objectKey, createFile(pCounters)));
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
	}
}