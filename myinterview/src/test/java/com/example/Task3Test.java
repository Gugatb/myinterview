package com.example;

import org.junit.Assert;
import org.junit.Test;

public class Task3Test {
	/**
	 * Get how many distinct.
	 * @author Gugatb
	 */
	@Test
	public void getHowManyDistinct1() {
		Task3 list = new Task3();
		list.add(1L, "Item A");
		list.add(2L, "Item B");
		list.add(3L, "Item C");
		list.add(4L, "Item D");
		list.add(5L, "Item A");
		list.add(6L, "Item E");
		list.add(7L, "Item A");
		list.add(8L, "Item F");
		
		Assert.assertTrue(list.getHowManyDistinct1() == 6);
	}
	
	/**
	 * Get how many distinct.
	 * @author Gugatb
	 */
	@Test
	public void getHowManyDistinct2() {
		Task3 list = new Task3();
		list.add(1L, "Item A");
		list.add(2L, "Item B");
		list.add(3L, "Item C");
		list.add(4L, "Item D");
		list.add(5L, "Item A");
		list.add(6L, "Item E");
		list.add(7L, "Item A");
		list.add(8L, "Item F");
		
		Assert.assertTrue(list.getHowManyDistinct2() == 5);
	}
}
