package com.example;

import org.junit.Assert;
import org.junit.Test;

public class Task1Test {
	/**
	 * Palindrome success.
	 * @author Gugatb
	 */
	@Test
	public void palindromeSuccess() {
		Assert.assertTrue(Task1.isPalindrome("madam"));
	}
	
	/**
	 * Palindrome failure.
	 * @author Gugatb
	 */
	@Test
	public void palindromeFailure() {
		Assert.assertFalse(Task1.isPalindrome("interview"));
	}
}
