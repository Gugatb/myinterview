package com.example;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class Task4Test {
	/**
	 * Get the counters.
	 * @author Gugatb
	 */
	@Test
	public void getCounters() {
		Map<String, Integer> counters = Task4.getCounters(
			"https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda"
		);
		
		Assert.assertTrue(counters.get("Female") != 0);
		Assert.assertTrue(counters.get("Male") != 0);
	}
}
