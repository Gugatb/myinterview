package com.example;

import org.junit.Assert;
import org.junit.Test;

import com.example.Task2.TaskItem;

public class Task2Test {
	/**
	 * Add the item.
	 * @author Gugatb
	 */
	@Test
	public void addItem() {
		Task2 list = new Task2();
		list.createList(10, 100, 20);
		list.add(100L, "Item N");
		Assert.assertTrue(list.size() == 21);
	}
	
	/**
	 * Remove the item.
	 * @author Gugatb
	 */
	@Test
	public void removeItem() {
		Task2 list = new Task2();
		list.createList(10, 100, 20);
		
		list.add(100L, "Item X");
		list.add(101L, "Item Y");
		list.add(102L, "Item X");
		list.add(103L, "Item W");
		
		TaskItem item = list.getItem(103L);
		Assert.assertTrue(list.remove(101L) == 1);
		Assert.assertTrue(list.remove("Item X") == 2);
		Assert.assertTrue(list.remove(item) == 1);
	}
}
